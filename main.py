from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from hurry.filesize import size

import shutil
import os.path
import time
import win32com.client
import sys
# import email library
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

class PMI(object):

    def __init__(self):
        self.chromeOptions = webdriver.ChromeOptions()
        self.dlFolder = "C:\Users\dlopes\Documents\Tasks\PMI"
        self.prefs = {"download.default_directory": self.dlFolder}

        self.chromeOptions.add_experimental_option("prefs", self.prefs)

        self.driver = webdriver.Chrome(chrome_options = self.chromeOptions)

        # 28 spaces
        #self.spacekeys = ['TPC', 'DRC']
        self.spacekeys = ['AW', 'BIOB', 'CC', 'COM', 'SWPRJ', 'DH', 'DV', 'DRC', 'DR', 'EnCo', 'IE', 'MIT', 'NIH', 'OMICS', 'PAL', 'PE', 'PPP', 'PW', 'PROT', 'RI', 'LAUN', 'SPAN', 'TF', 'TEM', 'TPC', 'VAMC', 'WIT', 'WG']
        self.start_time = time.time()

    # Login functions
    def login(self, username, password):

        self.driver.get("https://joinallofus.atlassian.net/login")

        emailFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('username'))
        passFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('password'))
        loginButtonElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="login"]'))

        emailFieldElement.clear()
        emailFieldElement.send_keys(username)
        passFieldElement.clear()
        passFieldElement.send_keys(password)
        loginButtonElement.click()

    def export(self):

        testSpace = {}
        successfulSpaces = {}
        timetobackup = {}
        failedSpaces = {}
        errorMessages = {}
        backupSize = {}
        totalsize = 0

        for x in range(0, len(self.spacekeys)):
            try:
                spacetime = time.time()

                # Open page to backup space
                self.driver.get("https://joinallofus.atlassian.net/wiki/spaces/exportspacewelcome.action?key="+self.spacekeys[x])

                # Get space name
                spacename = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="full-height-container"]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div[1]/a')).text
                print spacename
                # Choose to export as XML and click on radio button
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('format-export-format-xml')).click()

                # Click Next
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="space-tools-body"]/div/form/div/div/input')).click()

                # Click Full Export radio button
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('contentOptionAll')).click()

                time.sleep(5)

                # Click Export button
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="space-tools-body"]/div/form/div[2]/div/input')).click()

                # Confirm #title-text of page is Exporting Space - In Progress
                title_text = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('title-text')).text
                assert 'Exporting Space - In Progress' in title_text

                # Wait a max of 60 minutes for Download Link or else throw an exception
                try:
                    WebDriverWait(self.driver, 7200).until(EC.text_to_be_present_in_element((By.CLASS_NAME, "space-export-download-path"), "here"))
                finally:
                    print 'Download link available'

                # Click Download Link
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_class_name('space-export-download-path')).click()

                while not os.path.exists(self.dlFolder+"\Confluence-export.zip"):
                    time.sleep(1)

                tempSize = os.path.getsize(self.dlFolder+"\Confluence-export.zip")
                shutil.move(self.dlFolder+"\Confluence-export.zip", self.dlFolder+ '/' + self.spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip")

                totalSpaceTime = time.time() - spacetime
                totalsize += tempSize

                testSpace.update({spacename : {'status': '<span style="color: green">Success</span>', 'totaltime': str(time.strftime("%H:%M:%S", time.gmtime(totalSpaceTime))), 'backupsize': size(tempSize)}})
                #testSpace.update({spacename: {'totaltime': int(totalSpaceTime)}})
                #testSpace[spacename] = {'status' : '<span style="color: green">Success</span>'}
                print '---------------------------------------------------------'
                print testSpace
                print testSpace[spacename]['status']
                print testSpace[spacename]['totaltime']
                print totalsize
                #print totalSpaceTime
                # testSpace[spacename] = {'totaltime': int(totalSpaceTime)}
               #print testSpace


                #testSpace.update({})
                #testSpace[spacename]['status'] = '<span style="color: green">Success</span>'
                #testSpace[spacename]['backupsize'] = size(tempSize)
                #testSpace[spacename]['totaltime'] = totalSpaceTime

                #print self.dlFolder+ '/' + self.spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip"
                #print backupSize
                #print successfulSpaces


                #print os.path.getsize(self.dlFolder+ '/' + self.spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip")
                #print os.path.abspath(self.dlFolder+ '/' + self.spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip")
            except Exception, e:
                print e
                print sys.exc_info()[0]
                failedSpaces[self.spacekeys[x]] = '<span style="color: red">FAILED</span>'
                errorMessages[self.spacekeys[x]] = str(sys.exc_info()[0])

        # self.sendEmail(successfulSpaces, failedSpaces, errorMessages, timetobackup, backupSize)
        self.sendEmail(testSpace, failedSpaces, errorMessages, totalsize)

        self.driver.quit()

    def sendEmail(self, spaceinfo, failedSpaces, errorMessages, totalsize):
    #def sendEmail(self, successfulSpaces, failedSpaces, errorMessages, timetobackup, backupSize):
        elapsed_time = time.time() - self.start_time

        o = win32com.client.Dispatch("Outlook.Application")

        Msg = o.CreateItem(0)
        Msg.To = "dlopes@palladianpartners.com"

        Msg.Subject = "PMI Backup Summary - " + time.strftime("%m/%d/%Y")

        html = """\
        <html>
          <head>
          <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
            }
            th {
                text-align: left;
            }
            </style>
            </head>
          <body>
            <h1>PMI Backup Summary - """ + time.strftime("%m/%d/%Y") + """</h1>
            <p>Total time to complete backup: """ + str(time.strftime("%H:%M:%S", time.gmtime(elapsed_time))) + """</p>
            <p>Total size of backup: """ + size(totalsize) + """</p>"""

        html += """<p>The following spaces were backed up successfully:</p>
            <p>""" #+ ', ' . join(successfulSpaces)

        if len(spaceinfo):
        # if len(successfulSpaces):
            html += """<table>
                        <tr style="color: white; background-color: green;">
                        <th>Space Name</th>
                        <th>Status</th>
                        <th>Time to Complete</th>
                        <th>Backup Size</th>
                        </tr>"""
            for x in spaceinfo:
            # for x in successfulSpaces:
                html += """<tr>"""

                html += """<td>""" + x + """</td>"""
                html += """<td>""" + spaceinfo[x]['status'] + """</td>"""
                html += """<td>"""+ str(spaceinfo[x]['totaltime']) + """</td>"""
                html += """<td>""" + spaceinfo[x]['backupsize'] + """</td>"""
                html += """</tr>"""

            html += """</table>"""

        html += """</p>"""

        if len(failedSpaces):
            html+="""<p>The following spaces encountered errors:</p><p>"""
            for x in failedSpaces:
                html += x + ': ' + failedSpaces[x] + '<br>'

            html += """</p>"""

            html += """<p>Error Messages:<br>"""
            html += str(errorMessages)

            html += """</p>"""


        html += """</p>           
          </body>
        </html>
        """

        Msg.HTMLBody = html

        Msg.Send()



if __name__ == "__main__":
    test = PMI()
    test.login('dlopes', 'Dexterdoodle12')
    test.export()

