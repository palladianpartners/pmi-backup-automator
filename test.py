from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

import unittest

class LoginTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://joinallofus.atlassian.net/login")

    def test_Login(self):
        emailFieldID = "username"
        passFieldID = "password"
        fieldusername = 'dlopes'
        fieldpassword = 'Dexterdoodle12'
        loginButtonXPath = '//*[@id="login"]'

        emailFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id(emailFieldID))
        passFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id(passFieldID))
        loginButtonElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath(loginButtonXPath))

        emailFieldElement.clear()
        emailFieldElement.send_keys(fieldusername)
        passFieldElement.clear()
        passFieldElement.send_keys(fieldpassword)
        loginButtonElement.click()

    def backup_space(self):
        self.driver.get("https://joinallofus.atlassian.net/wiki/spaces/exportspacewelcome.action?key=AW")

   # def tearDown(self):
   #     self.driver.quit()


if __name__ == '__main__':
    unittest.main()
