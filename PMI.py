from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from hurry.filesize import size
from datetime import datetime, time

import shutil
import os.path
import time
import win32com.client
import sys
import requests
import xmlrpclib
import cookielib

# import email library
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText


class PMI(object):
    def __init__(self):
        open('C:\Users\dlopes\PycharmProjects\pmi-backup-automator\other.txt', 'w').write('this is a test')
        # Defines whether we are performing backup in the AM or PM
        self.meridiem = time.strftime('%p')

        # Default folders where downloads will be saved to
        self.dlFolder = "G:\PMI_ConfluenceBackup"
        self.tempFolder = "C:\Users\dlopes\PycharmProjects\pmi-backup-automator"

        # Download link that will be stored for downloading via chunks
        self.templink = ""

        # Set up directory structure (Date, AM/PM)
        self.setup_directory()

        # Set up Chrome Driver options
        self.chromeOptions = webdriver.ChromeOptions()
        self.prefs = {"download.default_directory": self.dlFolder}
        self.chromeOptions.add_experimental_option("prefs", self.prefs)
        self.driver = webdriver.Chrome(
            executable_path='C:\Users\dlopes\PycharmProjects\PMI-Backup-Automator\chromedriver.exe',
            chrome_options=self.chromeOptions)

        # The space keys that will need to be backed up (28 spaces in total)
        #self.spacekeys = ['TPC', 'DRC']
        #self.spacekeys = ['AW', 'BIOB', 'CC', 'COM', 'SWPRJ', 'DH', 'DV', 'DRC', 'DR', 'EnCo', 'IE', 'MIT', 'NIH', 'OMICS', 'PAL', 'PE', 'PPP', 'PW', 'PROT', 'RI', 'LAUN', 'SPAN', 'TF', 'TEM', 'TPC', 'VAMC', 'WIT', 'WG']
        self.start_time = time.time()
        self.totalsize = 0
        self.testSpace = {}
        self.failedSpaces = {}
        self.errorMessages = {}

        # wiki_url = "https://joinallofus.atlassian.net/wiki"  # your wiki url
        # server = xmlrpclib.ServerProxy(wiki_url + '/rpc/xmlrpc')
        # user, pw = "dlopes@palladianpartners.com", "Dexterdoodle12"  # your credentials
        # token = server.confluence2.login(user, pw)
        # print token


    # Login functions
    def login(self, username, password):

        self.driver.get("https://joinallofus.atlassian.net/login")

        emailFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('username'))
        loginButtonElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('login-submit'))

        emailFieldElement.clear()
        emailFieldElement.send_keys(username)
        loginButtonElement.click()
        time.sleep(3)

        passFieldElement = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('password'))
        passFieldElement.clear()
        passFieldElement.send_keys(password)

        time.sleep(3)
        loginButtonElement.click()

        #error = self.driver.find_element_by_class_name('error').text
        #print error
        #if error == 'Enter your password':

    def setup_directory(self):
        directory = self.dlFolder + '/' + time.strftime("%Y-%m-%d")
        print directory

        if not os.path.exists(directory):
            os.makedirs(directory)

        if os.path.exists(directory):
            subDirectory = directory + '/' + self.meridiem
            if not os.path.exists(subDirectory):
                os.makedirs(subDirectory)

        self.dlFolder = subDirectory

        print self.dlFolder

    def download_file(self, url):
        with requests.Session() as s:
            print self.data

            p = s.post('https://joinallofus.atlassian.net/login', data=self.data)
            #print p.text
            print 'after login'
            local_filename = url.split('/')[-1]
            print local_filename
            print url
            # NOTE the stream=True parameter
            r = s.get(url, stream=True)
            print 'after get url stream'
            print r.status_code
            # while r.status_code == 403:
            #     p = s.post('https://joinallofus.atlassian.net/login', data=self.data)
            #     r = s.get(url, stream=True)
            #     print r.status_code

            with open(local_filename, 'wb') as f:
                print 'opened!'
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)

            s.close()

        self.templink = local_filename

    def export(self, site, spacekeys, count = 0):
        print 'running export'
        time.sleep(5)
        fail = []

            #spacekeys = ['TPC', 'DRC']


            # spacekeys = ['CESVWG', 'GWG',]

        for x in range(0, len(spacekeys)):
            try:
                spacetime = time.time()

                # Open page to backup space
                if site == 'PMI':
                    self.driver.get(
                        "https://joinallofus.atlassian.net/wiki/spaces/exportspacewelcome.action?key=" + spacekeys[x])
                elif site == 'WGAP':
                    self.driver.get(
                        "https://wgapallofus.atlassian.net/wiki/spaces/exportspacewelcome.action?key=" + spacekeys[
                            x])

                # Get space name
                spacename = spacekeys[x]
                print spacename
                # Choose to export as XML and click on radio button
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('format-export-format-xml')).click()

                # Click Next
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_name('confirm')).click()

                # Click Full Export radio button
                WebDriverWait(self.driver, 10).until(
                    lambda driver: driver.find_element_by_id('contentOptionAll')).click()

                time.sleep(5)

                # Click Export button
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_name('confirm')).click()

                # Wait a max of 60 minutes for Download Link or else throw an exception
                try:
                    WebDriverWait(self.driver, 7200).until(
                        EC.text_to_be_present_in_element((By.CLASS_NAME, "space-export-download-path"), "here"))
                finally:
                    print 'Download link available'

                #with requests.Session() as s:
                hreflink = self.driver.find_element_by_class_name('space-export-download-path').get_attribute('href')

                #self.download_file(hreflink)
                # Click Download Link
                WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_class_name('space-export-download-path')).click()

                #while not os.path.exists(self.tempFolder + '/' + self.templink):
                while not os.path.exists(self.dlFolder + "\Confluence-export.zip"):
                    time.sleep(1)

                #tempSize = os.path.getsize(self.tempFolder + '/' + self.templink)
                #shutil.move(self.tempFolder + '/' + self.templink,
                #            self.dlFolder + '/' + self.spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip")
                #self.totalsize += tempSize

                tempSize = os.path.getsize(self.dlFolder+"\Confluence-export.zip")
                # print tempSize
                shutil.move(self.dlFolder+"\Confluence-export.zip", self.dlFolder+ '/' + spacekeys[x] + "_" + time.strftime("%Y-%m-%d") + ".zip")
                print 'finished moving file'
                totalSpaceTime = time.time() - spacetime
                print 'time so far: '
                print totalSpaceTime

                self.totalsize += tempSize
                print self.totalsize

                self.testSpace.update({spacename: {'status': '<span style="color: green">Success</span>',
                                                   'totaltime': str(
                                                       time.strftime("%H:%M:%S", time.gmtime(totalSpaceTime))),
                                                   'backupsize': size(tempSize)}})

                if spacekeys[x] in self.failedSpaces:
                    del self.failedSpaces[spacekeys[x]]

            except Exception, e:
                print e
                print sys.exc_info()[0]
                fail.append(spacekeys[x])
                self.failedSpaces[spacekeys[x]] = '<span style="color: red">FAILED</span>'
                self.errorMessages[spacekeys[x]] = str(sys.exc_info()[0])

        if(self.failedSpaces == 1):
            print self.failedSpaces
            self.export(site, fail, 1)

        if(count == 1):
            return

    def sendEmail(self, spaceinfo, failedSpaces, errorMessages, totalsize):
        # def sendEmail(self, successfulSpaces, failedSpaces, errorMessages, timetobackup, backupSize):
        elapsed_time = time.time() - self.start_time

        o = win32com.client.Dispatch("Outlook.Application")

        Msg = o.CreateItem(0)
        Msg.To = "dlopes@palladianpartners.com"

        Msg.Subject = "PMI Backup Summary - " + time.strftime("%m/%d/%Y")

        html = """\
        <html>
          <head>
          <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
            }
            th {
                text-align: left;
            }
            </style>
            </head>
          <body>
            <h1>PMI Backup Summary - """ + time.strftime("%m/%d/%Y") + """</h1>
            <p>Total time to complete backup: """ + str(time.strftime("%H:%M:%S", time.gmtime(elapsed_time))) + """</p>
            <p>Total size of backup: """ + size(totalsize) + """</p>"""

        html += """<p>The following spaces were backed up successfully:</p>
            <p>"""  # + ', ' . join(successfulSpaces)

        if len(spaceinfo):
            # if len(successfulSpaces):
            html += """<table>
                        <tr style="color: white; background-color: green;">
                        <th>Space Name</th>
                        <th>Status</th>
                        <th>Time to Complete</th>
                        <th>Backup Size</th>
                        </tr>"""
            for x in spaceinfo:
                # for x in successfulSpaces:
                html += """<tr>"""

                html += """<td>""" + x + """</td>"""
                html += """<td>""" + spaceinfo[x]['status'] + """</td>"""
                html += """<td>""" + str(spaceinfo[x]['totaltime']) + """</td>"""
                html += """<td>""" + spaceinfo[x]['backupsize'] + """</td>"""
                html += """</tr>"""

            html += """</table>"""

        html += """</p>"""

        if len(failedSpaces):
            html += """<p>The following spaces encountered errors:</p><p>"""
            for x in failedSpaces:
                html += x + ': ' + failedSpaces[x] + '<br>'

            html += """</p>"""

            html += """<p>Error Messages:<br>"""
            html += str(errorMessages)

            html += """</p>"""

        html += """</p>           
          </body>
        </html>
        """

        Msg.HTMLBody = html

        Msg.Send()

    def exportConfluence(self):
        time.sleep(5)

        spacetime = time.time()

        try:
            # Load Confluence main backup manager page
            self.driver.get("https://joinallofus.atlassian.net/wiki/plugins/servlet/ondemandbackupmanager/admin")

            # Click Create Backup button
            WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('submit')).click()

            # Wait a max of 120 minutes for Download Link or else throw an exception
            try:
                WebDriverWait(self.driver, 144000).until(
                    EC.text_to_be_present_in_element((By.CLASS_NAME, "smalltext"),
                                                     "This link will be available for 7 days since it was created."))
            finally:
                print 'Download link available'

            downloadlink = WebDriverWait(self.driver, 10).until(
                lambda driver: driver.find_element_by_xpath('//*[@id="backupLocation"]/a')).get_attribute("href")

            self.download_file(downloadlink)

            # Click Next
            WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="backupLocation"]/a')).click()

            #while not os.path.exists(self.tempFolder + '/' + self.templink):
            while not os.path.exists(self.dlFolder + "\Confluence-export.zip"):
                time.sleep(1)

            #tempSize = os.path.getsize(self.tempFolder + '/' + self.templink)
           # shutil.move(self.tempFolder + '/' + self.templink,
            #            self.dlFolder + '/Confluence_export_' + time.strftime("%Y-%m-%d") + ".zip")

            tempSize = os.path.getsize(self.dlFolder + "\Confluence-export.zip")
            # print tempSize
            shutil.move(self.dlFolder + "\Confluence-export.zip",
                        self.dlFolder + "/Confluence_export_" + time.strftime("%Y-%m-%d") + ".zip")
            print 'finished moving file'

            print tempSize
            totalSpaceTime = time.time() - spacetime
            self.totalsize += tempSize

            self.testSpace.update({'Confluence': {'status': '<span style="color: green">Success</span>',
                                                  'totaltime': str(
                                                      time.strftime("%H:%M:%S", time.gmtime(totalSpaceTime))),
                                                  'backupsize': size(tempSize)}})

        except Exception, e:
            print e
            print sys.exc_info()[0]

            o = win32com.client.Dispatch("Outlook.Application")

            Msg = o.CreateItem(0)
            Msg.To = "dlopes@palladianpartners.com"

            Msg.Subject = "CONFLUENCE ERROR - " + time.strftime("%m/%d/%Y")

            html = """\
                   <html>
                     <head>
                     <style>
                       table, th, td {
                           border: 1px solid black;
                           border-collapse: collapse;
                       }
                       th, td {
                           padding: 5px;
                       }
                       th {
                           text-align: left;
                       }
                       </style>
                       </head>
                     <body><p>"""
            html += e
            html += """</p>"""
            html += """<p>"""
            html += sys.exc_info()[0]
            html += """</p>"""

            html += """</p>           
                      </body>
                    </html>
                    """

            Msg.HTMLBody = html

            Msg.Send()
            print e
            print sys.exc_info()[0]

            #self.failedSpaces[self.spacekeys[100]] = '<span style="color: red">FAILED</span>'
            #self.errorMessages[self.spacekeys[100]] = str(sys.exc_info()[0])

    def exportJIRA(self):
        time.sleep(5)

        spacetime = time.time()

        try:
            # Load JIRA main backup manager page
            self.driver.get("https://joinallofus.atlassian.net/plugins/servlet/ondemandbackupmanager/admin")
            # Click Create Backup for Cloud button
            WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_id('submit-cloud')).click()

            # Wait a max of 120 minutes for Download Link or else throw an exception
            try:
                WebDriverWait(self.driver, 144000).until(
                    EC.text_to_be_present_in_element((By.ID, "backupLocation"), "jira_export"))
            finally:
                print 'Download link available'

            linktext = WebDriverWait(self.driver, 10).until(
                lambda driver: driver.find_element_by_xpath('//*[@id="backupLocation"]/a')).text
            print linktext

            downloadlink = WebDriverWait(self.driver, 10).until(
                lambda driver: driver.find_element_by_xpath('//*[@id="backupLocation"]/a')).get_attribute("href")

            self.download_file(downloadlink)


            # Click Download link
            WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="backupLocation"]/a')).click()

            #while not os.path.exists(self.tempFolder + '/' + self.templink):
            while not os.path.exists(self.dlFolder + '\\' + linktext):
                time.sleep(1)

            tempSize = os.path.getsize(self.tempFolder + '/' + self.templink)
            #tempSize = os.path.getsize(self.dlFolder + '\\' + linktext)
            shutil.move(self.tempFolder + '/' + self.templink,
                        self.dlFolder + '/JIRA_export_' + time.strftime("%Y-%m-%d") + ".zip")

            #shutil.move(self.dlFolder + '\\' + linktext,

            print tempSize
            self.totalsize += tempSize

            totalSpaceTime = time.time() - spacetime

            self.testSpace.update({'JIRA': {'status': '<span style="color: green">Success</span>',
                                            'totaltime': str(time.strftime("%H:%M:%S", time.gmtime(totalSpaceTime))),
                                            'backupsize': size(tempSize)}})

        except Exception, e:
            o = win32com.client.Dispatch("Outlook.Application")

            Msg = o.CreateItem(0)
            Msg.To = "dlopes@palladianpartners.com"

            Msg.Subject = "JIRA ERROR - " + time.strftime("%m/%d/%Y")

            html = """\
                   <html>
                     <head>
                     <style>
                       table, th, td {
                           border: 1px solid black;
                           border-collapse: collapse;
                       }
                       th, td {
                           padding: 5px;
                       }
                       th {
                           text-align: left;
                       }
                       </style>
                       </head>
                     <body><p>"""
            html += e
            html += """</p>"""
            html += """<p>"""
            html += sys.exc_info()[0]
            html += """</p>"""

            html += """</p>           
                      </body>
                    </html>
                    """

            Msg.HTMLBody = html

            Msg.Send()
            print e
            print sys.exc_info()[0]
            #self.failedSpaces[self.spacekeys[99]] = '<span style="color: red">FAILED</span>'
            #self.errorMessages[self.spacekeys[99]] = str(sys.exc_info()[0])

    def daily(self, site, spacekeys):
        self.export(site, spacekeys)
        self.sendEmail(self.testSpace, self.failedSpaces, self.errorMessages, self.totalsize)
        self.driver.quit()

    def weekly(self):
        self.exportJIRA()
        self.exportConfluence()
        self.export('PMI')
        self.export('WGAP')
        self.sendEmail(self.testSpace, self.failedSpaces, self.errorMessages, self.totalsize)
        self.driver.quit()
